import fs from "fs";

async function run () {
    const block = 'GUNPOWDER'

    let header = "Date,Price"//`${block}\nDate,Price`
    let csv = "";

    var raw_completed_market_orders = await fetch (`https://testapi.lokamc.com/completed_market_orders/search/findByType?type=${block}`, {
        method: "GET",
        headers: {
        }
        }).then(res => res.json())
    //console.log(raw_completed_market_orders)
    let completed_market_orders = raw_completed_market_orders._embedded.completed_market_orders

    for (let order in completed_market_orders) {
        let sale = completed_market_orders[order]

        //let seller = await get_playername(sale.ownerId)
        //let buyer = await get_playername(sale.buyerId)
        let raw_date = dateFromObjectId(sale.id)
        let date = /*parseInt(sale.id.substring(0, 8), 16) * 1000*/`${raw_date.getMonth()+1}/${raw_date.getDate()}/${raw_date.getFullYear()} ${raw_date.getUTCHours()}:${raw_date.getUTCMinutes()}`
        //console.log(date)
        //console.log(current_order)
        csv = csv + `\n${date},${sale.price}`
        //console.log(csv)
    }


    fs.writeFileSync(/*`data/${block}.csv`*/"current.csv", header + csv)
}

async function get_playername (UUID) {
    console.log(UUID)
    var player = await fetch(`https://testapi.lokamc.com/players/search/findByUuid?uuid=${UUID}`).then(res => res.json())
    console.log(player)
    return player;
}

function dateFromObjectId (objectId) {
	return new Date(parseInt(objectId.substring(0, 8), 16) * 1000);
};

run()